package mixins;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.util.Duration;

import org.controlsfx.control.Notifications;

/**
 *
 * Archivo Mix_Components.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:53 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Mix_Components {

  /**
   * 
   * @param width Anchura de la ventana
   * @param height Altura mínima de la ventana
   * @param title Titulo de la ventana
   * @param content Texto que incluirá el contendio principal
   * @return El componente confirm
   */
  public static Alert confirmComponent (double width, double height, String title, String content) {

    Alert alert = new Alert(
        AlertType.WARNING, 
        "", 
        ButtonType.YES, 
        ButtonType.NO, 
        ButtonType.CANCEL
      );

      
    alert.setTitle(title);
    alert.getDialogPane().setPadding(new Insets(10, 10, 10, 10));

    HBox box = new HBox(5);

    Label textContent = new Label(content);
    textContent.setWrapText(true);
    textContent.setPadding(new Insets(5, 5, 5, 5));
    textContent.setPrefWidth(width);

    box.getChildren().addAll(textContent);
    alert.getDialogPane().setContent(box);
    
    alert.getDialogPane().setPrefWidth(width);
    alert.getDialogPane().setMinHeight(height);

    alert.getDialogPane().setStyle("-fx-font-size: 13px");

    return alert;

  }

  public static Alert alertComponent (AlertType type, double width, double height, String title, String header, String content) {
    Alert alert = new Alert(type);
      
    alert.setTitle(title);
    alert.getDialogPane().setPadding(new Insets(10, 10, 10, 10));
    alert.setHeaderText(header);
    
    HBox box = new HBox(5);

    Label textContent = new Label(content);
    textContent.setWrapText(true);
    textContent.setPadding(new Insets(5, 5, 5, 5));
    textContent.setPrefSize(width, 40);

    box.getChildren().addAll(textContent);
    alert.getDialogPane().setContent(box);
    
    alert.getDialogPane().setPrefWidth(width);
    alert.getDialogPane().setMinHeight(height);

    alert.getDialogPane().setStyle("-fx-font-size: 13px");

    return alert;
  }
  
  /**
   * 
   * @param type Tipo de popOver (error, success, warning)
   * @param width Ancho que tendrá el componente
   * @param height Altura mínima que tendrá el componente
   * @param title Titulo que tendrá el popOver
   * @param content Contenido que tendrá el popOver
   * @return PopOver personalizado
   * @deprecated YA NO SE USAN POPOVERS
   */
  public static StackPane popOverContent (String type, double width, double height, String title, String content) {
  	StackPane layout = new StackPane();
  	
  	VBox group = new VBox();
  	group.setPrefWidth(width);
  	group.setMinHeight(height);
  	group.setPadding(new Insets(10, 10, 10, 10));
  	group.setAlignment(Pos.CENTER);
  	
  	Label titleGroup = new Label(title);
  	titleGroup.getStyleClass().add("popover__title");
  	titleGroup.setWrapText(true);
  	
  	Label contentGroup = new Label(content);
  	contentGroup.getStyleClass().add("popover__content");
  	contentGroup.setWrapText(true);
  	contentGroup.setAlignment(Pos.CENTER_LEFT);
  	
  	group.getChildren().addAll(titleGroup, contentGroup);
  	
  	switch (type) {
  		case "error":
  			group.getStyleClass().addAll("popover", "popover--error");
  			titleGroup.getStyleClass().add("popover__title--error");
  			contentGroup.getStyleClass().add("popover__content--error");
  			break;
  		case "success":
  			
  			break;
  		case "warning":
  			
  			break;
  	}
  	
  	layout.getChildren().add(group);

  	layout.getStylesheets().add(Mix_Components.class.getResource("/styles/comp/popover.css").toString());
  	
  	return layout;
  }
  
  /**
   * 
   * @param type Tipo de notificación
   * @param title Titulo de la notificación
   * @param content Contenido de la notificación
   */
  public static void notificationComponent (String type, String title, String content, int timer) {
  	
  	if (timer == 0) timer = 7;
  	
  	switch (type) {
  		case "normal":
  			Notifications.create().title(title).text(content).position(Pos.BASELINE_RIGHT).hideAfter(Duration.seconds(timer)).show();  			
  			break;
  		case "error":
  			Notifications.create().title(title).text(content).position(Pos.BASELINE_RIGHT).hideAfter(Duration.seconds(timer)).showError();  			  			
  			break;
  		case "warning":
  			Notifications.create().title(title).text(content).position(Pos.BASELINE_RIGHT).hideAfter(Duration.seconds(timer)).showWarning();  			
  			break;
  		case "success":
  			Notifications.create().title(title).text(content).position(Pos.BASELINE_RIGHT).hideAfter(Duration.seconds(timer)).showInformation();  			
  			break;
  	}
  	
  }

}