package mixins;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import tools.Tool_ReqJSON;

/**
 *
 * Archivo Mix_Lang.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:54 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Mix_Lang {

	/**
	 * 
	 * @param folder Nombre del folder contendedor del archivo json deseado
	 * @param jsonFile Nombre del archivo json a requerir
	 * @return Paquete de Idomas
	 */
	public static Tool_ReqJSON getlang (String folder, String jsonFile) {
		Tool_ReqJSON lang = null;
		
		try {
      lang = new Tool_ReqJSON(folder, jsonFile);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
		
		return lang;
	}
	
}
