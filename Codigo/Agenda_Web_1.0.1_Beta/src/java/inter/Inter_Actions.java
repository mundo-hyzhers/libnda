package inter;

/**
 *
 * Archivo Inter_Actions.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:51 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Inter_Actions {
	public void runAction ();
	public void newRegistry ();
	public void updatedRegistry ();
	public void deleteRegistry ();
}
