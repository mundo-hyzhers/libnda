package inter;

import exceptions.Exce_DAO_Exception;

/**
 *
 * Archivo Inter_TVDB.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:52 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Inter_TVDB<T> {

  public void Treatment ();
  public boolean Validate ();
  public boolean ValidateUpdate ();
  public boolean ValidateDataBase (int statusForm) throws Exce_DAO_Exception;
  public boolean DataBase() throws Exce_DAO_Exception;
  public boolean DataBaseUpdate () throws Exce_DAO_Exception;
  public boolean DataBaseDelete () throws Exce_DAO_Exception;
  public T getNewData();

}