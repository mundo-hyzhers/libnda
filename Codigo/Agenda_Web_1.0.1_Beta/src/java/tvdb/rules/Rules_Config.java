package tvdb.rules;

import javafx.scene.layout.StackPane;
import mixins.Mix_Components;

/**
 *
 * Archivo Rules_Config.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:57 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

@SuppressWarnings("unused")
public interface Rules_Config {

	public final double noticeWidth = 275;
	public final double noticeHeight = 100;
	
	/**
	 * 
	 * @param title
	 * @param error
	 * @return
	 * @deprecated
	 */
//	public static StackPane contentPopOver (String title, String error) {
//		return Mix_Components.popOverContent(
//				"error",
//				Rules_Config.noticeWidth,
//				Rules_Config.noticeHeight,
//				title,
//				error);
//	}
	
}
