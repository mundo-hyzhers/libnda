package tvdb;

import java.sql.Connection;

import dao.DAO_Management;
import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import inter.Inter_TVDB;
import mixins.Mix_Lang;
import tools.Tool_Treatment;
import tools.Tool_Validation;
import tvdb.rules.Rules_Category;

/**
 *
 * Archivo TVDB_Category.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:57 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public abstract class TVDB_Category implements Inter_TVDB<Data_Category> {
	
	private Data_Category newCategory, oldCategory;
	
	private DAO_Management manaDB = null;
		
	/**
	 * 
	 * @param conn Conexion a la base de datos
	 * @param newCategory Categoria para un nuevo registro
	 * @param oldCategory Categoria existente en la base de datos
	 */
	public TVDB_Category (Connection conn, Data_Category newCategory, Data_Category oldCategory) {
		this.manaDB = DAO_Management.init(conn);
		
		this.newCategory = newCategory;
		this.oldCategory = oldCategory;
	}
	
	@Override
	public void Treatment() {
		// Name
    String name = this.newCategory.getName();
    
    name = Tool_Treatment.trimTextAccents(name, Rules_Category.Treat_textName);

    this.newCategory.setName(name);
	}

	@Override
	public boolean Validate() {		
		Tool_Validation<Data_Category> val = new Tool_Validation<Data_Category>() {

			@Override
			public String noEqualObject(Data_Category original, Data_Category compare) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
		
		// Name
		String errorName = val.valEmptyMinMax(
				this.newCategory.getName(), 
				Rules_Category.Val_minName, 
				Rules_Category.Val_maxName);
		
		if (!errorName.equals("")) {
			this.noticeName(errorName);
			return false;
		} else this.clearNoticeName();
		
		return true;
	}
	
	@Override
	public boolean ValidateUpdate () {
		Tool_Validation<Data_Category> val = new Tool_Validation<Data_Category>() {

			@Override
			public String noEqualObject(Data_Category original, Data_Category compare) {
				int successVal = 0;
				
				if (this.noEqual(original.getName(), compare.getName(), "Category").equals("")) successVal++;
				
				if (successVal > 0) return "";
				return this.getError("valNoEqualObject");
			}
		
		};
		
		String errorCategory = val.noEqualObject(this.newCategory, this.oldCategory);
		
		if (!errorCategory.equals("")) {
			this.noticeNoChanges(errorCategory);
			
			return false;
		} else this.clearNoticeNoChanges();
		
		return true;
	}
	
	@Override
	public boolean ValidateDataBase(int statusForm) throws Exce_DAO_Exception {
		if (statusForm == 0 || statusForm == 1) {
			// Validar unique Name
			String errorUniqueName = Mix_Lang.getlang("tvdb", "Validation_esMx").getStringJSON("valCategoryUniqueName");
			
			if (this.manaDB.daoCategory().uniqueName(this.newCategory.getName())) {
				this.noticeName(errorUniqueName);
				return false;
			} else this.clearNoticeName();			
		} else if (statusForm == 2) {
			// Validar si la categoría esta siendo usada por alguna agenda
			String errorInUse = Mix_Lang.getlang("tvdb", "Validation_esMx").getStringJSON("valCategoryInUse");
			
			if (this.oldCategory == null) {
				this.noticeNoChanges(Mix_Lang.getlang("tvdb", "Validation_esMx").getStringJSON("valCategoryEmpty"));
				return false;
			} else this.clearNoticeNoChanges();
			
			if (this.manaDB.daoCategory().inUseCategory(this.oldCategory.getId())) {
				this.noticeName(errorInUse);
				return false;
			} else this.clearNoticeName();
		}
		
		return true;
	}

	@Override
	public boolean DataBase() throws Exce_DAO_Exception {
		System.out.println(this.newCategory);
		
		return false;
	}
	
	@Override
	public boolean DataBaseUpdate() throws Exce_DAO_Exception {
		Data_Category category = this.newCategory;
		category.setId(this.oldCategory.getId());
		
		return this.manaDB.daoCategory().update(category);
	}
	
	@Override
	public boolean DataBaseDelete() throws Exce_DAO_Exception {
		return this.manaDB.daoCategory().delete(this.oldCategory);
	}
	
	@Override
	public Data_Category getNewData() {
		return this.newCategory;
	}
	
	public abstract void noticeNoChanges(String error);
	public abstract void noticeName(String error);

	public abstract void clearNoticeNoChanges();
	public abstract void clearNoticeName();
}
