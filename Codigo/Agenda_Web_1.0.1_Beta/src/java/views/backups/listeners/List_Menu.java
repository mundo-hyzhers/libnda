package views.backups.listeners;

import views.backups.Comp_Backups;
import views.backups.mixins.Mix_Menu;

/**
 *
 * Archivo List_Menu.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:57 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class List_Menu {

	private static List_Menu listMenu = null;
	
	private Comp_Backups compBackups = Comp_Backups.init();
	
	public static List_Menu init () {
		if (List_Menu.listMenu == null)
			List_Menu.listMenu = new List_Menu();
		
		return List_Menu.listMenu;
	}
	
	private List_Menu () {
		new MenuButtons ();
	}
	
	private class MenuButtons {
		private MenuButtons () {
			this.backup();
			this.restore();
			this.exit();
		}
		
		private void backup () {
			List_Menu
				.this
				.compBackups
				.btn_backup
				.setOnAction(e -> {
					Mix_Menu.backupDB();
				});
		}
		
		private void restore () {
			List_Menu
				.this
				.compBackups
				.btn_restore
				.setOnAction(e -> {
					Mix_Menu.restore();
				});
		}
		
		private void exit () {
			List_Menu
				.this
				.compBackups
				.btn_exit
				.setOnAction(e -> {
					Mix_Menu.closeWindow();
				});
		}
	}
	
}
