package views.backups.scenes;

import inter.Inter_Scenes;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import mixins.Mix_Lang;
import tools.Tool_Image;
import tools.Tool_ReqJSON;
import views.backups.Comp_Backups;

/**
 *
 * Archivo Sce_Menu.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:58 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Sce_Menu implements Inter_Scenes {

	private static Sce_Menu sceneMenu = null;
	
	private double menuWidth = 0, menuHeight = 0;
	private int compHeight = 25;
	
	private Tool_ReqJSON dataTextlang = null;
	
	private Scene menuScene = null;
	
	private final Comp_Backups compBackups = Comp_Backups.init();
	
	private final String classDescriptions = "descriptions", classButtons = "buttons";
	
	public static Sce_Menu init (double menuWidth, double menuHeight) {
		if (Sce_Menu.sceneMenu == null)
			Sce_Menu.sceneMenu = new Sce_Menu(menuWidth, menuHeight);
		
		return Sce_Menu.sceneMenu;
	}
	
	private Sce_Menu (double menuWidth, double menuHeight) {
		this.menuWidth = menuWidth;
		this.menuHeight = menuHeight;
		
		this.dataTextlang = Mix_Lang.getlang("comp", "Text_esMx");
	}

	@Override
	public Scene buildingScene() {
		if (this.menuScene == null) {
			this.compBackups.layout_main.getStyleClass().add("window");
			this.compBackups.layout_main.setSpacing(10);
			this.compBackups.layout_main.setPrefSize(this.menuWidth, this.menuHeight);
			this.compBackups.layout_main.setAlignment(Pos.CENTER);
			
			this.compBackups.scroll_content.setPrefSize(this.menuWidth, this.menuHeight * .85);
			this.compBackups.scroll_content.getStyleClass().add("scroll");
			this.compBackups.scroll_content.setFitToWidth(true);
			this.compBackups.scroll_content.setFitToHeight(true);
			
			this.compBackups.val_db.setWrapText(true);
			this.compBackups.val_db.getStyleClass().addAll("validator", "validator--main");
			this.compBackups.val_db.setPrefWidth(this.menuWidth);
			this.compBackups.val_db.setMinHeight(0);
			this.compBackups.val_db.setVisible(false);
			this.compBackups.layout_content.getChildren().add(this.compBackups.val_db);
			
			this.compBackups.scroll_content.setContent(this.compBackups.layout_content);
			
			this.makeMenu();
			this.makeControl();
			
			this.compBackups.layout_main.getChildren().addAll(
					this.compBackups.scroll_content,
					this.compBackups.layout_control
					);
			
			this.menuScene = new Scene(this.compBackups.layout_main, this.menuWidth, this.menuHeight);
			this.menuScene.getStylesheets().add(this.getClass().getResource("/styles/sce/Backups_Menu.css").toString());
		}
		
		return this.menuScene;
	}
	
	private void makeMenu () {
		this.compBackups.layout_content.getStyleClass().add("window");
		this.compBackups.layout_content.setSpacing(10);
		this.compBackups.layout_content.setPrefSize(this.menuWidth, this.menuHeight * .90);
		this.compBackups.layout_content.setAlignment(Pos.CENTER);
		
		this.compBackups.backup_description.setText(this.dataTextlang.getStringJSON("txtBackupDescription"));
		this.compBackups.backup_description.setPrefSize(this.menuWidth, this.compHeight * 2.5);
		this.compBackups.backup_description.setWrapText(true);
		this.compBackups.backup_description.getStyleClass().add(this.classDescriptions);
		this.compBackups.layout_content.getChildren().add(this.compBackups.backup_description);
		
		this.compBackups.btn_backup.setText(this.dataTextlang.getStringJSON("txtBackup"));
		this.compBackups.btn_backup.setPrefSize(this.menuWidth * .75, this.compHeight);
		this.compBackups.btn_backup.getStyleClass().add(this.classButtons);
		this.compBackups.layout_content.getChildren().add(this.compBackups.btn_backup);

		this.compBackups.restore_description.setText(this.dataTextlang.getStringJSON("txtRestoreDescription"));
		this.compBackups.restore_description.setPrefSize(this.menuWidth, this.compHeight * 2.5);
		this.compBackups.restore_description.setWrapText(true);
		this.compBackups.restore_description.getStyleClass().add(this.classDescriptions);
		this.compBackups.layout_content.getChildren().add(this.compBackups.restore_description);
		
		this.compBackups.btn_restore.setText(this.dataTextlang.getStringJSON("txtRestore"));
		this.compBackups.btn_restore.setPrefSize(this.menuWidth * .75, this.compHeight);
		this.compBackups.btn_restore.getStyleClass().add(this.classButtons);
		this.compBackups.layout_content.getChildren().add(this.compBackups.btn_restore);
	}
	
	private void makeControl () {
		this.compBackups.layout_control.getStyleClass().add("window");
		this.compBackups.layout_control.setSpacing(10);
		this.compBackups.layout_control.setPrefSize(this.menuWidth, this.menuHeight * .10);
		this.compBackups.layout_control.setAlignment(Pos.CENTER);
		
		this.compBackups.btn_exit.setGraphic(new ImageView(Tool_Image.getImage("app", "salir", "png")));
		this.compBackups.btn_exit.setPrefSize(this.menuWidth * .25, this.compHeight * 2);
		this.compBackups.btn_exit.getStyleClass().addAll(this.classButtons, "buttons--exit");
		this.compBackups.layout_control.getChildren().add(this.compBackups.btn_exit);
	}
	
}
