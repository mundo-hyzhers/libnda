package views.category.actions;

import java.sql.Connection;

import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import inter.Inter_Actions;
import javafx.scene.control.Control;
import tools.Tool_ConnectionDB;
import tvdb.TVDB_Category;
import views.View_Home;
import views.category.Comp_FormCategory;
import views.category.mixins.Mix_FormCategory;
import views.home.mixins.Mix_Board;
import views.registry.mixins.Mix_FormAgenda;

/**
 *
 * Archivo Act_Category.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:58 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Act_Category implements Inter_Actions {

	private Comp_FormCategory compFormCategory = Comp_FormCategory.init();
	
	private Data_Category category;
	
	public Act_Category (Data_Category category) {
		this.category = category;
		
		this.runAction();
	}
	
	public Act_Category () {
		this.category = Mix_FormCategory.catchData();	
		
		this.runAction();
	}
	
	@Override
	public void runAction() {
		switch (this.compFormCategory.getActionForm()) {
			case 0:
				
				break;
			case 1:
				this.updatedRegistry();
				break;
				
			case 2:
				this.deleteRegistry();
				break;
		}
	}
	
	@Override
	public void newRegistry() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatedRegistry() {
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();
		
		try {
			if (this.category != null) {
				TVDB_Category workingCategory = this.workingCategory(conn.getConnection(), this.category, this.compFormCategory.getOldDataCategory());
				workingCategory.Treatment();
				
				Mix_FormCategory.clearValidators();
				
				if (workingCategory.ValidateUpdate()) {
					
					if (workingCategory.Validate()) {
						
						if (workingCategory.ValidateDataBase(1)) {
							
							if (workingCategory.DataBaseUpdate()) {
								Mix_FormCategory.chargeCategories(conn.getConnection());
								
								Mix_FormAgenda.chargeCategories(conn.getConnection());
								
								View_Home winHome = View_Home.init(null);
								
								Mix_Board.refreshResume(winHome.getWindow().getWidth(), winHome.getWindow().getHeight(), null);
								
								if (this.compFormCategory.getActionForm() != 0)
									Mix_FormCategory.statusForm(0);
									
							}
						}						
					}
				}
				
			}
		} catch (Exce_DAO_Exception e) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
	}
	
	@Override
	public void deleteRegistry() {
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();
		
		try {
			TVDB_Category workinCategory = this.workingCategory(conn.getConnection(), null, this.compFormCategory.getOldDataCategory());
			
			if (workinCategory.ValidateDataBase(2)) {
				if (workinCategory.DataBaseDelete()) {
					Mix_FormCategory.chargeCategories(conn.getConnection());
					
					Mix_FormAgenda.chargeCategories(conn.getConnection());					
					
					if (this.compFormCategory.getActionForm() != 0)
						Mix_FormCategory.statusForm(0);
				}
			}
			
		} catch (Exce_DAO_Exception e) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
	}
	
	private TVDB_Category workingCategory (Connection conn, Data_Category newCategory, Data_Category oldCategory) {
		TVDB_Category workingCategory = new TVDB_Category(conn, newCategory, oldCategory) {
			
			@Override
			public void noticeNoChanges(String error) {
				Act_Category.this.compFormCategory.val_noChanges.setMinHeight(Control.USE_PREF_SIZE);
				Act_Category.this.compFormCategory.val_noChanges.setText(error);
				Act_Category.this.compFormCategory.val_noChanges.setVisible(true);
			}
			
			@Override
			public void noticeName(String error) {
				Act_Category.this.compFormCategory.val_name.setMinHeight(Control.USE_PREF_SIZE);
				Act_Category.this.compFormCategory.val_name.setText(error);
				Act_Category.this.compFormCategory.val_name.setVisible(true);
			}
			
			@Override
			public void clearNoticeNoChanges() {
				Act_Category.this.compFormCategory.val_noChanges.setMinHeight(0);
				Act_Category.this.compFormCategory.val_noChanges.setText("");
				Act_Category.this.compFormCategory.val_noChanges.setVisible(false);
			}
			
			@Override
			public void clearNoticeName() {								
				Act_Category.this.compFormCategory.val_name.setMinHeight(0);
				Act_Category.this.compFormCategory.val_name.setText("");
				Act_Category.this.compFormCategory.val_name.setVisible(false);
			}

		};
		
		return workingCategory;
	}

}
