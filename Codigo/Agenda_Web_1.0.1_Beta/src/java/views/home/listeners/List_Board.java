package views.home.listeners;


import data.Data_Agenda;
import javafx.collections.ObservableList;
//import javafx.animation.PauseTransition;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import mixins.Mix_Reports;
import tools.Tool_ConnectionDB;
import views.View_Backups;
import views.View_Category;
import views.View_Home;
import views.View_Registry;
import views.backups.mixins.Mix_Menu;
import views.category.mixins.Mix_FormCategory;
import views.home.Comp_Board;
import views.home.mixins.Mix_Board;
import views.home.mixins.Mix_Table;
import views.registry.mixins.Mix_FormAgenda;

/**
 *
 * Archivo List_Board.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:00 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class List_Board {

  private static List_Board listBoard = null;
  
  private Comp_Board compBoard = Comp_Board.init();
  
  /**
   * 
   * @return Instancía única del listener
   */
  public static List_Board init () {
    if (List_Board.listBoard == null)
      List_Board.listBoard = new List_Board();

    return List_Board.listBoard;
  }

  private List_Board () {
    new List_Controls();
  }

  private class List_Controls {

    private List_Controls() {
    	this.searchingBoard();
    	this.generateReport();
      this.openWindowNew();
      this.openCategory();
      this.openBackups();
      this.exitApp();
    }
    
    private void searchingBoard () {
    	// FUNCIÓN CON DEBOUNCE PARA CONTROLAR EL NÚMERO DE PETICIONES A LA BASE DE DATOS. FUNCIONA BIEN PERO NO TANTO COMO ME GUSTARÍA
//    	PauseTransition pt = new PauseTransition(Duration.millis(2000));
//    	
//    	List_Board
//    		.this
//    		.compBoard
//    		.cont_search_text
//    		.setOnKeyPressed(e -> {
//    			if (pt.getStatus().toString().equals("STOPPED"))
//    				pt.playFromStart();
//    			
//    			pt.setOnFinished(e2 -> {
//    				String data = List_Board.this.compBoard.cont_search_text.getText();
//  					
//    				View_Home viewHome = View_Home.init(null);
//    				
//  					if (!data.isEmpty()) {
//  						Tool_ConnectionDB conn = Tool_ConnectionDB.init();
//  						
//  						try {
//  							DAO_Management manaDB = DAO_Management.init(conn.getConnection());
//  							
//  							System.out.println(data);
//  							
//  							Mix_Board.refreshResume(viewHome.getWindow().getWidth(), viewHome.getWindow().getHeight(), manaDB.daoAgenda().searchAgenda(data));
//  						} catch (Conf_DAO_Exception e3) {
//  							e3.printStackTrace();
//  						} finally {
//  							conn.closeConnection();
//  						}
//  					} else Mix_Board.refreshResume(viewHome.getWindow().getWidth(), viewHome.getWindow().getHeight(), null);
//    			});
//    			
//    			
//    		});
    	
    	List_Board
			.this
			.compBoard
			.cont_search_text
			.setOnKeyPressed(e -> {
				String data = List_Board.this.compBoard.cont_search_text.getText();
				
				View_Home viewHome = View_Home.init(null);
				
				if (!data.isEmpty()) {					
					ObservableList<Data_Agenda> filterAgendas = null;
					
					filterAgendas = List_Board.this.compBoard.getListAgendas().filtered(agenda -> {
						String pattern = data.toLowerCase() + ".*|.*" + data.toLowerCase();
						// Buscar por el alias presenta el problema de que algunas agendas no contienen un alias por lo que se tiene el problema de NULL
						return agenda.getEmail().toLowerCase().matches(pattern) || agenda.getWeb().toLowerCase().matches(pattern) || agenda.getDataCategory().getName().toLowerCase().matches(pattern);
					});
					
					Mix_Board.refreshResume(viewHome.getWindow().getWidth(), viewHome.getWindow().getHeight(), filterAgendas);
					
//					Tool_ConnectionDB conn = Tool_ConnectionDB.init();					
//					
//					try {
//						DAO_Management manaDB = DAO_Management.init(conn.getConnection());
//						
//						Mix_Board.refreshResume(viewHome.getWindow().getWidth(), viewHome.getWindow().getHeight(), manaDB.daoAgenda().searchAgenda(data + e.getText()));
//					} catch (Exce_DAO_Exception e3) {
//						e3.printStackTrace();
//					} finally {
//						conn.closeConnection();
//					}
				} else Mix_Board.refreshResume(viewHome.getWindow().getWidth(), viewHome.getWindow().getHeight(), List_Board.this.compBoard.getListAgendas());
				
			});
    }
    
    private void generateReport () {
    	List_Board
    		.this
    		.compBoard
    		.cont_buttons_report
    		.setOnAction(e -> {
    			Mix_Reports.generateReport("agendas");
    		});
    }
    
    private void openWindowNew () {
    	List_Board
    		.this
    		.compBoard
    		.cont_buttons_openRegistry
    		.setOnAction(e -> {
    			View_Home winHome = View_Home.init(null);
        
    			View_Registry viewReg = View_Registry.init();
    			Stage winReg = viewReg.getWindow();
        
    			Mix_FormAgenda.cleanForm();
    			Mix_Table.cleanSearch();
    			Mix_Table.currentTable();
    			
    			try {
    				winReg.initOwner(winHome.getWindow());						
					} catch (Exception e2) {
					}
    			
    			winReg.show();
    			
    			Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds(); 
    			winReg.setX((primaryScreenBounds.getWidth() + winReg.getWidth() * 2.1) / 2);
    			winReg.setY((primaryScreenBounds.getHeight() - winReg.getHeight()) / 2.5);
    			
    			winHome.getWindow().setWidth(505);
    			winHome.chargeScene("table");
    			
    			Mix_FormAgenda.statusForm(0);
    			Mix_FormAgenda.clearValidators(true);
    			Mix_FormAgenda.clearValidators(false);

      });
    }
    
    private void openCategory () {
    	List_Board
    		.this
    		.compBoard
    		.cont_buttons_openCategory
    		.setOnAction(e -> {
    			View_Home winHome = View_Home.init(null);
    			
    			View_Category viewCategory = View_Category.init();
    			Stage winCategory = viewCategory.getWindow();
    			
    			try {
    				winCategory.initOwner(winHome.getWindow());						
					} catch (Exception e2) {
					}
    			
    			Tool_ConnectionDB conn = Tool_ConnectionDB.init();
    			
    			try {
    				Mix_FormCategory.chargeCategories(conn.getConnection());
					} catch (Exception e2) {
						// TODO: handle exception
					}
    			
    			Mix_FormCategory.cleanForm();
    			Mix_FormCategory.statusForm(0);
    			Mix_FormCategory.clearValidators();
    			Mix_Board.enableDisableControls(true);
    			winCategory.showAndWait();
    			// TODO : Aqui se deberá desactivar los botones de la ventana board
    			
//    			Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds(); 
//    			winCategory.setX((primaryScreenBounds.getWidth() + winCategory.getWidth() * 1.5) / 2);
//    			winCategory.setY((primaryScreenBounds.getHeight() - winCategory.getHeight()) / 2.5);
    		});
    }
    
    private void openBackups () {
    	List_Board
    		.this
    		.compBoard
    		.cont_buttons_dataBase
    		.setOnAction(e -> {
    			View_Home viewHome = View_Home.init(null);
    			
    			View_Backups viewBack = View_Backups.init();
    			Stage winBack = viewBack.getWindow();
    			
    			try {
    				winBack.initOwner(viewHome.getWindow());    				
    			} catch (Exception e2) {
    			}
    			
    			Mix_Menu.clearValidator();
    			Mix_Board.enableDisableControls(true);
    			
    			winBack.show();
    		});
    }
    
    private void exitApp () {
    	List_Board
    		.this
    		.compBoard
    		.cont_buttons_exit
    		.setOnAction(e -> System.exit(0));
    }

  }

} 