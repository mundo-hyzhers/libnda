package views.home.scenes;

import inter.Inter_Scenes;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import mixins.Mix_Lang;
import tools.Tool_Image;
import tools.Tool_ReqJSON;
import views.home.Comp_Board;
import views.home.mixins.Mix_Board;

/**
 *
 * Archivo Sce_Board.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:01 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Sce_Board implements Inter_Scenes {

	private static Sce_Board sceneBoard = null;
	
	private double homeWidth = 0, homeHeight = 0;
	private int fieldsHeight = 20;
	
	private String classControlsText = "board__controls__text-field", classControlButton = "board__controls__button";
	
	private Tool_ReqJSON dataAgendaLang = null, buttonsLang = null, textLang = null;
	
	private Scene homeScene = null;
	
	private Comp_Board compBoard = Comp_Board.init();
	
	/**
	 * 
	 * @param homeWidth Anchura de la ventana home
	 * @param homeHeight Altura de la ventana home
	 * @return Instancía Sce_Board
	 */
	public static Sce_Board init (double homeWidth, double homeHeight) {
		if (Sce_Board.sceneBoard == null)
			Sce_Board.sceneBoard = new Sce_Board(homeWidth, homeHeight);
		
		return Sce_Board.sceneBoard;
	}
	
	private Sce_Board (double homeWidth, double homeHeight) {
		this.homeWidth = homeWidth;
		this.homeHeight = homeHeight;
		
		this.dataAgendaLang = Mix_Lang.getlang("data", "Agenda_esMx");
		this.buttonsLang = Mix_Lang.getlang("comp", "Buttons_esMx");
		this.textLang = Mix_Lang.getlang("comp", "Text_esMx");
	}
	
	@Override
	public Scene buildingScene() {
		if (this.homeScene == null) {
			this.compBoard.main_layout.getStyleClass().add("board");
			this.compBoard.main_layout.setSpacing(10);
			this.compBoard.main_layout.setPrefSize(this.homeWidth, this.homeHeight);
			
			// Área Resumen
			this.compBoard.res_container.getStyleClass().add("board__resume");
			this.compBoard.res_container.setId("boardResume");
			this.compBoard.res_container.setPrefWidth(this.homeWidth);
			
			Mix_Board.refreshResume(this.homeWidth, this.homeHeight, null);
			
			this.compBoard.res_scroll.setPrefSize(this.homeWidth, this.homeHeight * .75);
			this.compBoard.res_scroll.setFitToWidth(true);
			this.compBoard.res_scroll.getStyleClass().add("board__resume--scroll");
			this.compBoard.res_scroll.setContent(this.compBoard.res_container);
			
			// Área Controles
			this.controlsSearch();
			
			// Área de Información del creador
      this.infoAuthor();
      
			this.compBoard.main_layout.getChildren().addAll(
					this.compBoard.res_scroll,
					this.compBoard.cont_container,
          this.compBoard.info_container
			);
			
			
			this.homeScene = new Scene(this.compBoard.main_layout, this.homeWidth, this.homeHeight);
			this.homeScene.getStylesheets().add(this.getClass().getResource("/styles/sce/Home_Board.css").toString());
		}
		
		return this.homeScene;
	}
	
	private void controlsSearch () {
		this.compBoard.cont_container.setHgap(10);
		this.compBoard.cont_container.setVgap(10);
		this.compBoard.cont_container.getStyleClass().add("board__controls");
		this.compBoard.cont_container.setPrefSize(this.homeWidth, this.homeHeight * .25);
		
		this.compBoard.cont_search_text.setPrefSize(this.homeWidth, this.fieldsHeight);
		this.compBoard.cont_search_text.getStyleClass().add(this.classControlsText);
		this.compBoard.cont_search_text.setPromptText(this.textLang.getStringJSON("txtSearch") + " " + this.dataAgendaLang.getStringJSON("webTitle") + ", " + this.dataAgendaLang.getStringJSON("categoryTitle") + ", " + this.dataAgendaLang.getStringJSON("emailTitle"));
		this.compBoard.cont_search_text.setLeft(new ImageView(Tool_Image.getImage("app", "buscar_con_mano", "png")));
		this.compBoard.cont_container.add(this.compBoard.cont_search_text, 0, 1, 5, 1);
		
		this.compBoard.cont_buttons_report.setGraphic(new ImageView(Tool_Image.getImage("app", "reporte", "png")));
		this.compBoard.cont_buttons_report.getStyleClass().add("board__controls__button--icon");
		this.compBoard.cont_buttons_report.setPrefSize(this.homeWidth * .30, this.homeHeight * 2);
		this.compBoard.cont_container.add(this.compBoard.cont_buttons_report, 0, 2);
		
		this.compBoard.cont_buttons_dataBase.setGraphic(new ImageView(Tool_Image.getImage("app", "base_datos", "png")));
		this.compBoard.cont_buttons_dataBase.getStyleClass().add("board__controls__button--icon");
		this.compBoard.cont_buttons_dataBase.setPrefSize(this.homeWidth * .30, this.homeHeight * 2);
		this.compBoard.cont_container.add(this.compBoard.cont_buttons_dataBase, 1, 2);
		
		this.compBoard.cont_buttons_openCategory.setGraphic(new ImageView(Tool_Image.getImage("app", "categoria", "png")));
		this.compBoard.cont_buttons_openCategory.getStyleClass().add("board__controls__button--icon");
		this.compBoard.cont_buttons_openCategory.setPrefSize(this.homeWidth * .30	, this.fieldsHeight * 2);
		this.compBoard.cont_container.add(this.compBoard.cont_buttons_openCategory, 2, 2);
		
		this.compBoard.cont_buttons_openRegistry.setText("+ " + this.buttonsLang.getStringJSON("btnAgenda"));
		this.compBoard.cont_buttons_openRegistry.getStyleClass().add(this.classControlButton);
		this.compBoard.cont_buttons_openRegistry.setPrefSize(this.homeWidth * .40, this.fieldsHeight);
		this.compBoard.cont_container.add(this.compBoard.cont_buttons_openRegistry, 3, 2);
		
		this.compBoard.cont_buttons_exit.setGraphic(new ImageView(Tool_Image.getImage("app", "salir", "png")));
		this.compBoard.cont_buttons_exit.getStyleClass().add("board__controls__button--icon");
		this.compBoard.cont_buttons_exit.setPrefSize(this.homeWidth * .30, this.fieldsHeight * 2);
		this.compBoard.cont_container.add(this.compBoard.cont_buttons_exit, 4, 2);
		
		Mix_Board.enableDisableControls();
	}
	
	public void infoAuthor () {
	  this.compBoard.info_container.setSpacing(10);
	  
	  this.compBoard.info_copyright.setText(this.textLang.getStringJSON("txtCopyright") + " " + this.textLang.getStringJSON("txtAuthor"));
	  this.compBoard.info_copyright.setWrapText(true);
	  this.compBoard.info_copyright.getStyleClass().add("board__info__text");
	  this.compBoard.info_copyright.setPrefSize(this.homeWidth, this.fieldsHeight);
	  this.compBoard.info_copyright.setAlignment(Pos.CENTER_RIGHT);
	  
	  this.compBoard.info_email.setText(this.textLang.getStringJSON("txtEmail"));
	  this.compBoard.info_email.getStyleClass().addAll("board__info__text", "board__info__text--email");
	  this.compBoard.info_email.setPrefWidth(this.homeWidth);
	  this.compBoard.info_email.setAlignment(Pos.CENTER_RIGHT);
	  
	  this.compBoard.info_container.getChildren().addAll(
	    this.compBoard.info_copyright,
      this.compBoard.info_email
    );
  }
	
}