package views.home;

import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Label;

/**
 *
 * Archivo Comp_Information.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:01 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Comp_Information {

	// Main Scroll
	public ScrollPane main_scroll = new ScrollPane();
	
	// Main layout
	public VBox main_layout = new VBox();
	
	// Área de cajas
		// Caja Web
		public HBox box_web = new HBox();
		public Label box_web_title = new Label();
		public Label box_web_text = new Label();
		
		// Caja Alias
		public HBox box_alias = new HBox();
		public Label box_alias_title = new Label();
		public Label box_alias_text = new Label();
		
		// Caja Email
		public HBox box_email = new HBox();
		public Label box_email_title = new Label();
		public Label box_email_text = new Label();

		// Caja Password
		public HBox box_password = new HBox();
		public Label box_password_title = new Label();
		public Label box_password_text = new Label();

		// Caja Category
		public HBox box_category = new HBox();
		public Label box_category_title = new Label();
		public Label box_category_text = new Label();

		// Caja Details
		public HBox box_details = new HBox();
		public Label box_details_title = new Label();
		public Label box_details_text = new Label();
	
	private static Comp_Information compInformation = null;
	
	public static Comp_Information init () {
		
		if (Comp_Information.compInformation == null)
			Comp_Information.compInformation = new Comp_Information();
		
		return Comp_Information.compInformation;
			
	}
	
	private Comp_Information () {}
	
}
