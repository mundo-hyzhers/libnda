package views.home;

import org.controlsfx.control.table.TableRowExpanderColumn;
import org.controlsfx.control.textfield.CustomTextField;

import data.Data_Agenda;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * Archivo Comp_Table.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:02 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Comp_Table {
	//Variables
	private ObservableList<Data_Agenda> obsDataAgenda = FXCollections.observableArrayList();

	// Main layout
	public VBox main_layout = new VBox();
	
	// Área de la tabla
		public TableView<Data_Agenda> tab_main = new TableView<>();
		public TableColumn<Data_Agenda, String> tab_main_id = new TableColumn<>();
		public TableColumn<Data_Agenda, String> tab_main_user = new TableColumn<>();
		public TableColumn<Data_Agenda, String> tab_main_web = new TableColumn<>();
		public TableColumn<Data_Agenda, String> tab_main_alias = new TableColumn<>();
		public TableColumn<Data_Agenda, String> tab_main_email = new TableColumn<>();
		public TableColumn<Data_Agenda, String> tab_main_password = new TableColumn<>();
		public TableColumn<Data_Agenda, String> tab_main_category = new TableColumn<>();
		public TableColumn<Data_Agenda, String> tab_main_details = new TableColumn<>();
		public TableRowExpanderColumn<Data_Agenda> tab_main_expander = null;
		
		
	// Área de busqueda
		public HBox sea_area = new HBox();
		public CustomTextField sea_area_text = new CustomTextField();
		
	// Área de control
		// Botones
		public HBox cont_btns = new HBox();
		public Button cont_btns_new = new Button();
		public Button cont_btns_edit = new Button();
		public Button cont_btns_delete = new Button();
		
		private static Comp_Table compTable = null;
		
		public static Comp_Table init () {
			if (Comp_Table.compTable == null)
				Comp_Table.compTable = new Comp_Table();
			
			return Comp_Table.compTable;
		}
		
		private Comp_Table () {
			this.obsDataAgenda.clear();
						
			this.tab_main.setItems(this.obsDataAgenda);
		}

		public ObservableList<Data_Agenda> getObsDataAgenda() {
			return obsDataAgenda;
		}
		
}
