package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.parser.ParseException;

import dao.base.Base_CategoryAgenda_DAO;
import dao.sql.SQL_Agenda;
import dao.sql.SQL_Category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import javafx.collections.ObservableList;
import data.Data_Agenda;
import mixins.Mix_Components;
import tools.Tool_ReqJSON;
import tools.Tool_Text;

/**
 *
 * Archivo DAO_CategoryAgenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:49 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class DAO_CategoryAgenda implements Base_CategoryAgenda_DAO {
	
	private Tool_ReqJSON lang, langComponents;
	
	private Connection conn;
	
	public DAO_CategoryAgenda(Connection conn) {
		this.conn = conn;
		
		try {
      this.lang = new Tool_ReqJSON("dao" , "Exceptions_esMx");
      this.langComponents = new Tool_ReqJSON("comp", "Notification_esMx");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
	}

	@Override
	public boolean create(Data_Category data) throws Exce_DAO_Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ObservableList<Data_Category> read() throws Exce_DAO_Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(Data_Category data) throws Exce_DAO_Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Data_Category data) throws Exce_DAO_Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean transactionCreate(Data_Category category, Data_Agenda agenda) throws Exce_DAO_Exception {
		PreparedStatement psNewCategory = null, psNewAgenda = null;
		ResultSet rsNewCategory = null, rsNewAgenda = null;
		
		try {
			this.conn.setAutoCommit(false);
			
			// Se crea la categoría
			psNewCategory = this.conn.prepareStatement(SQL_Category.CREATE, Statement.RETURN_GENERATED_KEYS);
			psNewCategory.setString(1, category.getName());
			
			if (psNewCategory.executeUpdate() == 0)
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Categoría", this.lang.getStringJSON("sqlNoCreated")));
			
			rsNewCategory = psNewCategory.getGeneratedKeys();
			
			if (rsNewCategory.next()) {
				category.setId(rsNewCategory.getInt("id"));
				
				if (category.getId() != -1 && category.getId() > 0) {
					// Se crea la agenda
					psNewAgenda = this.conn.prepareStatement(SQL_Agenda.CREATE);
					psNewAgenda.setString(1, agenda.getWeb());
					psNewAgenda.setString(2, agenda.getAlias());
					psNewAgenda.setString(3, agenda.getEmail());
					psNewAgenda.setString(4, agenda.getPassword());
					psNewAgenda.setInt(5, category.getId());
					psNewAgenda.setString(6, agenda.getDetails());
					
					if (psNewAgenda.executeUpdate() == 0)
						throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Agenda", this.lang.getStringJSON("sqlNoCreated")));
					
					Mix_Components.notificationComponent(
							"normal", 
							this.langComponents.getStringJSON("newAgendaTitle"),
							Tool_Text.findAndReplaceAll("##", agenda.getWeb(), this.langComponents.getStringJSON("agendaContentWeb")) + "\n"
							+ Tool_Text.findAndReplaceAll("##", agenda.getEmail(), this.langComponents.getStringJSON("agendaContentEmail")) + "\n"
							+ Tool_Text.findAndReplaceAll("##", agenda.getAlias() != null ? agenda.getAlias() : ":(" , this.langComponents.getStringJSON("agendaContentAlias")) + "\n"
							+ Tool_Text.findAndReplaceAll("##", category.getName(), this.langComponents.getStringJSON("agendaContentCategory")),
							0
					);
					
					this.conn.commit();
				}
			}			
			
			this.conn.rollback();
		} catch (SQLException e) {
			try {
				this.conn.rollback();				
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Rollback", this.lang.getStringJSON("sqlRollback")), e2);
			}
			
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (psNewCategory != null)
					psNewCategory.close();
			
				if (rsNewCategory != null)
					rsNewCategory.close();
				
				if (psNewAgenda != null)
					psNewAgenda.close();
				
				if (rsNewAgenda != null)
					rsNewAgenda.close();
			} catch (SQLException e3) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Cierres", this.lang.getStringJSON("sqlClose")), e3);
			}
		}
	
		try {
			if (psNewAgenda.isClosed()) return true;
			
			return false;			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Return", this.lang.getStringJSON("sqlReturn")), e);
		}
		
	}

	@Override
	public void setConnection(Connection conn) {
		this.conn = conn;
	}

	@Override
	public boolean transactionUpdate(Data_Category category, Data_Agenda agenda) throws Exce_DAO_Exception {
		PreparedStatement psNewCategory = null, psNewAgenda = null;
		ResultSet rsNewCategory = null, rsNewAgenda = null;
		
		try {
			this.conn.setAutoCommit(false);
			
			// Se crea la categoría
			psNewCategory = this.conn.prepareStatement(SQL_Category.CREATE, Statement.RETURN_GENERATED_KEYS);
			psNewCategory.setString(1, category.getName());
			
			if (psNewCategory.executeUpdate() == 0)
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Categoría", this.lang.getStringJSON("sqlNoCreated")));
			
			rsNewCategory = psNewCategory.getGeneratedKeys();
			
			if (rsNewCategory.next()) {
				category.setId(rsNewCategory.getInt("id"));
				
				if (category.getId() != -1 && category.getId() > 0) {
					// Se actualiza la agenda
					psNewAgenda = this.conn.prepareStatement(SQL_Agenda.UPDATE);
					psNewAgenda.setString(1, agenda.getWeb());
					psNewAgenda.setString(2, agenda.getAlias());
					psNewAgenda.setString(3, agenda.getEmail());
					psNewAgenda.setString(4, agenda.getPassword());
					psNewAgenda.setInt(5, category.getId());
					psNewAgenda.setString(6, agenda.getDetails());
					psNewAgenda.setInt(7, agenda.getId());
					
					if (psNewAgenda.executeUpdate() == 0)
						throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Agenda", this.lang.getStringJSON("sqlNoUpdated")));
					
					Mix_Components.notificationComponent(
							"normal", 
							this.langComponents.getStringJSON("updateAgendaTitle"),
							Tool_Text.findAndReplaceAll("##", agenda.getWeb(), this.langComponents.getStringJSON("agendaContentWeb")) + "\n"
							+ Tool_Text.findAndReplaceAll("##", agenda.getEmail(), this.langComponents.getStringJSON("agendaContentEmail")) + "\n"
							+ Tool_Text.findAndReplaceAll("##", agenda.getAlias() != null ? agenda.getAlias() : ":(" , this.langComponents.getStringJSON("agendaContentAlias")) + "\n"
							+ Tool_Text.findAndReplaceAll("##", category.getName(), this.langComponents.getStringJSON("agendaContentCategory")),
							0
					);
					
					this.conn.commit();
				}
			}			
			
			this.conn.rollback();
		} catch (SQLException e) {
			try {
				this.conn.rollback();				
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Rollback", this.lang.getStringJSON("sqlRollback")), e2);
			}
			
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (psNewCategory != null)
					psNewCategory.close();
			
				if (rsNewCategory != null)
					rsNewCategory.close();
				
				if (psNewAgenda != null)
					psNewAgenda.close();
				
				if (rsNewAgenda != null)
					rsNewAgenda.close();
			} catch (SQLException e3) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Cierres", this.lang.getStringJSON("sqlClose")), e3);
			}
		}
	
		try {
			if (psNewAgenda.isClosed()) return true;
			
			return false;			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Transacción Return", this.lang.getStringJSON("sqlReturn")), e);
		}
		
	}
	
}
