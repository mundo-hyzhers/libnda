package dao.sql;

/**
 *
 * Archivo SQL_Agenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:48 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface SQL_Agenda {

	public final String CREATE = "INSERT INTO agenda_web (web, alias, email, password, category, details) VALUES (?, ?, ?, ?, ?, ?)";
	public final String READ = "SELECT "
			+ "ag.id, ag.user, ag.web, ag.alias, ag.email, ag.password, ag.category, ag.details, "
			+ "c.name "
			+ "FROM agenda_web ag "
			+ "INNER JOIN category c ON ag.category = c.id "
			+ "ORDER BY ag.web ASC";
	public final String SEARCH = "SELECT "
			+ "ag.id, ag.user, ag.web, ag.alias, ag.email, ag.password, ag.category, ag.details, "
			+ "c.name "
			+ "FROM agenda_web ag "
			+ "INNER JOIN category c ON ag.category = c.id "
			+ "WHERE MATCH (ag.web, ag.alias, ag.email) AGAINST (? IN NATURAL LANGUAGE MODE) "
			+ "ORDER BY ag.web ASC";
	public final String UPDATE = "UPDATE agenda_web SET web = ?, alias = ?, email = ?, password = ?, category = ?, details = ? WHERE id = ?";
	public final String DELETE = "DELETE FROM agenda_web WHERE id = ?";
}
