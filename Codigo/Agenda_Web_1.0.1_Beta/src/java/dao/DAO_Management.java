package dao;

import java.sql.Connection;

import dao.DAO_Agenda;
import dao.DAO_Category;
import dao.DAO_CategoryAgenda;

/**
 *
 * Archivo DAO_Management.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:49 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class DAO_Management {

	private static DAO_Management DAOManagement = null;

	private DAO_Agenda DAOAgenda = null;
	private DAO_Category DAOCategory = null;
	private DAO_CategoryAgenda DAOCategoryAgenda = null;
	
	private Connection conn = null;
	
	public static DAO_Management init (Connection conn) {
		if (DAO_Management.DAOManagement == null)
			DAO_Management.DAOManagement = new DAO_Management(conn);
		else DAO_Management.DAOManagement.conn = conn;
		
		return DAO_Management.DAOManagement;
	}
	
	private DAO_Management (Connection conn) {
		this.conn = conn;
	}
	
	public DAO_Agenda daoAgenda () {
		if (this.DAOAgenda == null)
			this.DAOAgenda = new DAO_Agenda(this.conn);
		else this.DAOAgenda.setConnection(this.conn); 
			
		return this.DAOAgenda;
	}

	public DAO_Category daoCategory () {
		if (this.DAOCategory == null)
			this.DAOCategory = new DAO_Category(this.conn);
		else this.DAOCategory.setConnection(this.conn);
		return this.DAOCategory;
	}

	public DAO_CategoryAgenda daoCategoryAgenda () {
		if (this.DAOCategoryAgenda == null)
			this.DAOCategoryAgenda = new DAO_CategoryAgenda(this.conn);
		else this.DAOCategoryAgenda.setConnection(this.conn);
		
		return this.DAOCategoryAgenda;
	}
	
}
