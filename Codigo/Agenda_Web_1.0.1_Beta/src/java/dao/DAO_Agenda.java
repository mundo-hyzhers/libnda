package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import org.json.simple.parser.ParseException;

import dao.base.Base_Agenda;
import dao.sql.SQL_Agenda;

import data.Data_Agenda;
import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import mixins.Mix_Components;

import tools.Tool_ReqJSON;
import tools.Tool_Text;

/**
 *
 * Archivo DAO_Agenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:48 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class DAO_Agenda implements  Base_Agenda {
	
	private Tool_ReqJSON lang, langComponents;
	private Connection conn;
	
	public DAO_Agenda (Connection conn) {
		this.conn = conn;
	
		try {
      this.lang = new Tool_ReqJSON("dao" , "Exceptions_esMx");
      this.langComponents = new Tool_ReqJSON("comp", "Notification_esMx");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
	}
	
	@Override
	public boolean create(Data_Agenda data) throws Exce_DAO_Exception {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement(SQL_Agenda.CREATE);	
			ps.setString(1, data.getWeb());
			ps.setString(2, data.getAlias());
			ps.setString(3, data.getEmail());
			ps.setString(4, data.getPassword());
			ps.setInt(5, data.getCategory());
			ps.setString(6, data.getDetails());
			
			if (ps.executeUpdate() == 0)
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Crear Agenda", this.lang.getStringJSON("sqlNoCreated")));
			
			Mix_Components.notificationComponent(
					"normal", 
					this.langComponents.getStringJSON("newAgendaTitle"),
					Tool_Text.findAndReplaceAll("##", data.getWeb(), this.langComponents.getStringJSON("agendaContentWeb")) + "\n"
					+ Tool_Text.findAndReplaceAll("##", data.getEmail(), this.langComponents.getStringJSON("agendaContentEmail")) + "\n"
					+ Tool_Text.findAndReplaceAll("##", data.getAlias() != null ? data.getAlias() : ":(", this.langComponents.getStringJSON("agendaContentAlias")),
					0
			);
			
			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Crear", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();				
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Crear Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		try {
			if (ps.isClosed()) return true;
			return false;
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Crear Return", this.lang.getStringJSON("sqlReturn")), e);
		}
		
	}
	
	@Override
	public ObservableList<Data_Agenda> read() throws Exce_DAO_Exception {
		ObservableList<Data_Agenda> agendas = FXCollections.observableArrayList();
		
		Statement st = null;
		ResultSet rs = null;
		
		try {
			
			st = this.conn.createStatement();
			rs = st.executeQuery(SQL_Agenda.READ);
			
			while (rs.next()) {
				Data_Agenda currentAgenda = new Data_Agenda();
				
				currentAgenda.setId(rs.getInt("ag.id"));
				currentAgenda.setWeb(rs.getString("ag.web"));
			
				currentAgenda.setAlias(rs.getString("ag.alias"));
//				rs.getString("ag.alias");
//				if (!rs.wasNull())
//					currentAgenda.setAlias(rs.getString("ag.alias"));
//				else currentAgenda.setAlias("");
			
				currentAgenda.setEmail(rs.getString("ag.email"));
				currentAgenda.setPassword(rs.getString("ag.password"));
				
				currentAgenda.setDetails(rs.getString("ag.details"));
//				rs.getString("ag.details");
//				if (!rs.wasNull())
//					currentAgenda.setDetails(rs.getString("ag.details"));
//				else currentAgenda.setDetails("");
				
				currentAgenda.setDataCategory(new Data_Category(
						rs.getInt("ag.category"),
						rs.getString("c.name")
						));
				
				agendas.add(currentAgenda);
			}
			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Leer Agenda", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (st != null)
					st.close();
				
				if (rs != null)
					rs.close();
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Leer Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
			
		return agendas;
	}
	
	@Override
	public ObservableList<Data_Agenda> searchAgenda(String data) throws Exce_DAO_Exception {
		ObservableList<Data_Agenda>	agendas = FXCollections.observableArrayList();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.conn.prepareStatement(SQL_Agenda.SEARCH);
			ps.setString(1, data);
			
			rs = ps.executeQuery();
			
			String agAlias = null, agDetails = null;

			while (rs.next()) {				
				rs.getString("ag.alias");
				if (!rs.wasNull())
					agAlias = rs.getString("ag.alias");
				else agAlias = "";
				
				rs.getString("ag.details");
				if (!rs.wasNull())
					agDetails = rs.getString("ag.details");
				else agDetails = "";
				
				Data_Agenda currentAgenda = new Data_Agenda(
						rs.getInt("ag.id"),
						rs.getInt("ag.user"),
						rs.getString("ag.web"),
						agAlias,
						rs.getString("ag.email"),
						rs.getString("ag.password"),
						rs.getInt("ag.category"),
						agDetails
						);
				
				currentAgenda.setDataCategory( new Data_Category(
						rs.getInt("ag.category"),
						rs.getString("c.name")
						) );
				
				agendas.add(currentAgenda);
			}
			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Buscar Agenda", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				
				if (rs != null)
					rs.close();
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Buscar Agenda Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		return agendas;
	}

	@Override
	public boolean update(Data_Agenda data) throws Exce_DAO_Exception {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement(SQL_Agenda.UPDATE);	
			ps.setString(1, data.getWeb());
			ps.setString(2, data.getAlias());
			ps.setString(3, data.getEmail());
			ps.setString(4, data.getPassword());
			ps.setInt(5, data.getCategory());
			ps.setString(6, data.getDetails());
			ps.setInt(7, data.getId());
			
			if (ps.executeUpdate() == 0)
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Update Agenda", this.lang.getStringJSON("sqlNoUpdated")));
			
			Mix_Components.notificationComponent(
					"normal", 
					this.langComponents.getStringJSON("updateAgendaTitle"),
					Tool_Text.findAndReplaceAll("##", data.getWeb(), this.langComponents.getStringJSON("agendaContentWeb")) + "\n"
					+ Tool_Text.findAndReplaceAll("##", data.getEmail(), this.langComponents.getStringJSON("agendaContentEmail")) + "\n"
					+ Tool_Text.findAndReplaceAll("##", data.getAlias() != null ? data.getAlias() : ":(", this.langComponents.getStringJSON("agendaContentAlias")),
					0
			);
			
			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Update", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();				
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Update Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		try {
			if (ps.isClosed()) return true;
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Update Return", this.lang.getStringJSON("sqlReturn")), e);
		}
		
		return false;
	}

	@Override
	public boolean delete(Data_Agenda data) throws Exce_DAO_Exception {
		PreparedStatement ps = null;
		
		try {
			
			ps = this.conn.prepareStatement(SQL_Agenda.DELETE);
			
			ps.setInt(1, data.getId());
			
			if (ps.executeUpdate() == 0)
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Eliminar Agenda", this.lang.getStringJSON("sqlNoDeleted")));
			
			Mix_Components.notificationComponent(
					"normal", 
					this.langComponents.getStringJSON("deleteAgendaTitle"),
					Tool_Text.findAndReplaceAll("##", data.getWeb(), this.langComponents.getStringJSON("agendaContentWeb")) + "\n"
					+ Tool_Text.findAndReplaceAll("##", data.getEmail(), this.langComponents.getStringJSON("agendaContentEmail")) + "\n"
					+ Tool_Text.findAndReplaceAll("##", data.getAlias() != null ? data.getAlias() : ":(", this.langComponents.getStringJSON("agendaContentAlias")),
					0
			);
			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Eliminar", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Eliminar Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		try {
			if (ps.isClosed()) return true;
					
			return false;
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Eliminar Return", this.lang.getStringJSON("sqlReturn")), e);
		}
	}

	@Override
	public void setConnection(Connection conn) {
		this.conn = conn;
	}

}
