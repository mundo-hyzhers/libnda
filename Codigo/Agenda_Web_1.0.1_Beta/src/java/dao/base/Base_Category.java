package dao.base;

import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import inter.Inter_CRUD_DAO;

/**
 *
 * Archivo Base_Category.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:47 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Base_Category extends Inter_CRUD_DAO<Data_Category> {

	public boolean uniqueName (String name) throws Exce_DAO_Exception;
	public boolean inUseCategory (int id) throws Exce_DAO_Exception;
	
}
