package data;

/**
 *
 * Archivo Data_Agenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:50 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Data_Agenda {

  private int id = -1;
  private int user = -1;
  private String web = null;
  private String alias = null;
  private String email = null;
  private String password = null;
  private int category = -1;
  private String details = null;

  private Data_Category dataCategory = null;
  
  public Data_Agenda (int id, int user, String web, String alias, String email, String password, int category, String details) {
    this.id = id;
    this.user = user;
    this.web = web;
    this.alias = alias;
    this.email = email;
    this.password = password;
    this.category = category;
    this.details = details;    
  }

  public Data_Agenda () {}

  /**
   * @return the id
   */
  public int getId() {
    return id;
  }
  /**
   * @param id the id to set
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the user
   */
  public int getUser() {
    return user;
  }
  /**
   * @param user the user to set
   */
  public void setUser(int user) {
    this.user = user;
  }

  /**
   * @return the web
   */
  public String getWeb() {
    return web;
  }
  /**
   * @param web the web to set
   */
  public void setWeb(String web) {
    this.web = web;
  }

  /**
   * @return the alias
   */
  public String getAlias() {
    return alias;
  }
  /**
   * @param alias the alias to set
   */
  public void setAlias(String alias) {
    this.alias = alias;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }
  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }
  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return the category
   */
  public int getCategory() {
    return category;
  }
  /**
   * @param category the category to set
   */
  public void setCategory(int category) {
    this.category = category;
  }

  /**
   * @return the details
   */
  public String getDetails() {
    return details;
  }
  /**
   * @param details the details to set
   */
  public void setDetails(String details) {
    this.details = details;
  }
  
  /**
   * 
   * @return the dataCategory
   */
  public Data_Category getDataCategory () {
  	return this.dataCategory;
  }
  
  /**
   * 
   * @param dataCategory the dataCategory to set
   */
  public void setDataCategory (Data_Category dataCategory) {
  	this.dataCategory = dataCategory;
  }

  @Override
  public String toString() {
    return "" +
      "==============\n" +
      "  Agenda = " + this.id + " :\n" +
      "    User = " + this.user + "\n" +
      "     Web = " + this.web + "\n" +
      "   Alias = " + this.alias + "\n" +
      "   Email = " + this.email + "\n" +
      "Password = " + this.password + "\n" +
      "Category = " + this.category + "\n" +
      "Details  = " + this.details + "\n" +
      "==============\n";
  }

}